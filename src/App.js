import './App.css';
import "./App.css";
import "./reset.css";
import { Route, Switch } from 'react-router-dom';
import React, { Component } from 'react';
import Header from './components/Header';
import Themes from './components/Themes';
import Battle from './components/Battle'
import Result from './components/Result';
class App extends Component {

  state = {
    theme: true,
    resultData: null
  };

  themeChanger = () => {
    this.setState({ theme: !this.state.theme })
  }


  // updateResultData = (data) => {
  //   this.setState({
  //     resultData: data
  //   })
  // }

  render() {
    console.log(this.state)
    return (
      <div className={this.state.theme ? "light" : "dark"}>
        <div className='container-light'>

          <Themes
            theme={this.themeChanger}
            onThemeChange={this.state.theme} />
          <Switch>

            <Route exact path="/" render={((props) => <Header onThemeChange={this.state.theme} />)}>
            </Route>

            <Route exact path="/battle" render={((props) => <Battle onInput={this.handleInput} updateResultData={this.updateResultData} onThemeChange={this.state.theme} />)}>
            </Route>

            <Route exact path="/result/:user1/:user2" render={((props) => <Result onThemeChange={this.state.theme} {...props} />)}>
            </Route>

          </Switch>
        </div>
      </div>
    );
  }
}

export default App;