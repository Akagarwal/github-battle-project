import React, { Component } from "react";

class Section extends Component {

    render() {
        return (
            <div className="section">
                {this.props.cards.map((cards, index) => {
                    return (
                        <div className={this.props.onThemeChange ? "card-light" : "card-dark"}>
                            <h4 className="card-h4">#{index + 1}</h4>
                            <img className="card-img" src={cards.owner.avatar_url} alt="card" />
                            <h2 className="card-h2"><a className="card-a" href="name">{cards.owner.login}</a></h2>
                            <ul className="card-ul">

                                <li className="card-ul-li">
                                    <img className="card-ul-li-img" src="logos/login.png" alt="Login" />
                                    <a href="theme" className={this.props.onThemeChange ? "a-light" : "a-dark"} >{cards.owner.login}</a>
                                </li>

                                <li className="card-ul-li">
                                    <img className="card-ul-li-img" src="logos/star.png" alt="Star" />
                                    {cards.watchers_count} stars
                                </li>

                                <li className="card-ul-li">
                                    <img className="card-ul-li-img" src="logos/fork.png" alt="Fork" />
                                    {cards.forks_count} forks
                                </li>

                                <li className="card-ul-li">
                                    <img className="card-ul-li-img" src="logos/issues.png" alt="Issues" />
                                    {cards.open_issues_count} open issues
                                </li>
                            </ul>
                        </div>
                    )
                })}
            </div>
        );
    }
}

export default Section;