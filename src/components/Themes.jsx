import React, { Component } from "react";
import { NavLink } from 'react-router-dom';

class Themes extends Component {
    state = {}


    render() {
        return (
            <header>
                <nav className="battle-nav">
                    <ul className="battle-nav-ul">
                        <li className="battle-nav-ul-li">
                            <input name="nav-input" type="radio" id="Popular" />
                            <label
                                htmlFor="Popular">
                                <NavLink
                                    className={this.props.onThemeChange ? 'blacktext' : 'whitetext'}
                                    activeStyle={{ color: "brown" }}
                                    exact={true}
                                    to="/">Popular</NavLink>
                            </label>
                        </li>
                        <li className="battle-nav-ul-li">
                            <input type="radio" name="nav-input" id="Battle" />
                            <label
                                htmlFor="Battle">
                                <NavLink
                                    className={this.props.onThemeChange ? 'blacktext' : 'whitetext'}
                                    activeStyle={{ color: "brown" }}
                                    to="/battle">Battle</NavLink>
                            </label>
                        </li>
                    </ul>
                    <button onClick={this.props.theme} className="battle-torch">{this.props.onThemeChange ? '🔦' : '💡'}</button>
                </nav>
            </header >
        );
    }
}

export default Themes;

// style={{ color: "rgb(187, 46, 31)" }}

// style={{ color: this.props.onThemeChange ? 'black' : 'white' }}