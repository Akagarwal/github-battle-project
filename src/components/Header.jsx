import React, { Component } from "react";
import Section from "./Section";

class Header extends Component {

    state = {
        popularData: []
    }

    componentDidMount = () => {
        fetch("https://api.github.com/search/repositories?q=stars:%3E1+language:All&sort=stars&order=desc&type=Repositories")
            .then((res) => res.json())
            .then((json) => {
                this.setState({
                    popularData: json.items
                });
            })
    }

    handleLangChange = (language) => {
        fetch(`https://api.github.com/search/repositories?q=stars:%3E1+language:${language}&sort=stars&order=desc&type=Repositories`)
            .then((res) => res.json())
            .then((json) => {
                this.setState({
                    popularData: json.items
                });
            })
    }

    render() {
        return (
            <React.Fragment>
                <header>
                    {/* <nav className="nav">
                        <ul className="nav-ul1">
                            <li className="nav-ul1-li">
                                <input name="nav-input" type="radio" id="Popular" />
                                <label htmlFor="Popular"><Link style={{ color: "rgb(187, 46, 31)" }} className="Links" to="/">Popular</Link></label>
                            </li>
                            <li className="nav-ul1-li">
                                <input type="radio" name="nav-input" id="Battle" />
                                <label htmlFor="Battle"><Link style={{ color: this.props.onThemeChange ? 'black' : 'white' }} className="Links" to="/battle">Battle</Link></label>
                            </li>
                        </ul>
                        <button onClick={this.props.theme} className="torch">{this.props.onThemeChange ? '🔦' : '💡'}</button>
                    </nav> */}
                    <ul className="nav-ul2">
                        <li>
                            <input name="language" id="All" type="radio" />
                            <label onClick={() => this.handleLangChange("All")} id="All-label" htmlFor="All">All</label>
                        </li>
                        <li>
                            <input name="language" id="JavaScript" type="radio" />
                            <label onClick={() => this.handleLangChange("JavaScript")} htmlFor="JavaScript">JavaScript</label>
                        </li>
                        <li>
                            <input name="language" id="Ruby" type="radio" />
                            <label onClick={() => this.handleLangChange("Ruby")} htmlFor="Ruby">Ruby</label>
                        </li>
                        <li>
                            <input name="language" id="Java" type="radio" />
                            <label onClick={() => this.handleLangChange("Java")} htmlFor="Java">Java</label>
                        </li>
                        <li>
                            <input name="language" id="CSS" type="radio" />
                            <label onClick={() => this.handleLangChange("CSS")} htmlFor="CSS">CSS</label>
                        </li>
                        <li>
                            <input name="language" id="Python" type="radio" />
                            <label onClick={() => this.handleLangChange("Python")} htmlFor="Python">Python</label>
                        </li>
                    </ul>
                </header>

                <Section
                    cards={this.state.popularData}
                    onThemeChange={this.props.onThemeChange}
                />
            </React.Fragment>
        );
    }
}

export default Header;