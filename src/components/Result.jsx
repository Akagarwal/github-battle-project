import React, { Component } from "react";
import { Link } from "react-router-dom";
class Result extends Component {

    state = {
        data: [],
        user1Score: "",
        user2Score: "",
        user1status: "",
        user2status: "",
    }

    componentDidMount() {
        Promise.all([fetch(`https://api.github.com/users/${this.props.match.params.user1}`), fetch(`https://api.github.com/users/${this.props.match.params.user2}`)])

            .then(([res1, res2]) => {
                return Promise.all([res1.json(), res2.json()])
            })
            .then(([res1, res2]) => {
                this.setState({
                    data: [res1, res2],
                    user1Score: res1.public_repos + res1.followers - res1.following,
                    user2Score: res2.public_repos + res2.followers - res2.following
                })
                let userData1 = res1.public_repos + res1.followers - res1.following;
                let userData2 = res2.public_repos + res2.followers - res2.following;

                if (userData1 > userData2) {
                    this.setState({
                        user1status: "Winner",
                        user2status: "Loser"
                    })
                } else if (userData2 > userData1) {
                    this.setState({
                        user1status: "Loser",
                        user2status: "Winner"
                    })
                } else {
                    this.setState({
                        user1status: "Tie",
                        user2status: "Tie"
                    })
                }
            }
            )
    }

    // winnerLoser = () => {

    //     let userData1 = this.state.user1Score
    //     let userData2 = this.state.user2Score

    //     if (userData1 > userData2) {
    //         this.setState({
    //             user1status: "Winner",
    //             user2status: "Loser"
    //         })
    //     } else if (userData2 > userData1) {
    //         this.setState({
    //             user1status: "Loser",
    //             user2status: "Winner"
    //         })
    //     } else {
    //         this.setState({
    //             user1status: "Tie",
    //             user2status: "Tie"
    //         })
    //     }
    // }


    render() {
        console.log(this.state);
        console.log(this.props);
        if (this.state.data.length !== 2) {
            return "Loading data";
        } else {
            console.log(this.state.data.length)

            return (
                < React.Fragment >

                    <div className="result-outermost-div">
                        <div className="result-outer-div" >
                            <div className="result-cards-div" style={{ backgroundColor: this.props.onThemeChange ? 'rgba(0, 0, 0, 0.08)' : 'rgb(36, 40, 42)' }}>
                                <h1 className="result-cards-1-h4">{this.state.user1status}</h1>
                                <img className="result-cards-img" src={this.state.data[0].avatar_url} alt="Avatar" />
                                <h4 className="result-cards-2-h4">Score: {this.state.user1Score}</h4>
                                <h2 className="result-cards-h2">{this.state.data[0].login}</h2>
                                <ul className="result-cards-ul">
                                    <li className="result-cards-li">
                                        <img className="card-ul-li-img" src="/logos/resultuser.png" alt="Login" />
                                        <a href="theme" className={this.props.onThemeChange ? "a-light" : "a-dark"} >{this.state.data[0].name}</a>
                                    </li>

                                    <li className="result-cards-li">
                                        <img className="card-ul-li-img" src="/logos/followers.png" alt="Star" />
                                        {this.state.data[0].followers} followers
                                    </li>

                                    <li className="result-cards-li">
                                        <img className="card-ul-li-img" src="/logos/following.png" alt="Fork" />
                                        {this.state.data[0].following} following
                                    </li>

                                    <li className="result-cards-li">
                                        <img className="card-ul-li-img" src="/logos/repositories.png" alt="Issues" />
                                        {this.state.data[0].public_repos} repositories
                                    </li>
                                </ul>
                            </div>

                            <div className="result-cards-div" style={{ backgroundColor: this.props.onThemeChange ? 'rgba(0, 0, 0, 0.08)' : 'rgb(36, 40, 42)' }}>
                                <h1 className="result-cards-1-h4">{this.state.user2status}</h1>
                                <img className="result-cards-img" src={this.state.data[1].avatar_url} alt="Avatar" />
                                <h4 className="result-cards-2-h4">Score: {this.state.user2Score}</h4>
                                <h2 className="result-cards-h2">{this.state.data[1].login}</h2>
                                <ul className="result-cards-ul">
                                    <li className="result-cards-li">
                                        <img className="card-ul-li-img" src="/logos/resultuser.png" alt="Login" />
                                        <a href="theme" className={this.props.onThemeChange ? "a-light" : "a-dark"} >{this.state.data[1].name}</a>
                                    </li>

                                    <li className="result-cards-li">
                                        <img className="card-ul-li-img" src="/logos/followers.png" alt="Star" />
                                        {this.state.data[1].followers} followers
                                    </li>

                                    <li className="result-cards-li">
                                        <img className="card-ul-li-img" src="/logos/following.png" alt="Fork" />
                                        {this.state.data[1].following} following
                                    </li>

                                    <li className="result-cards-li">
                                        <img className="card-ul-li-img" src="/logos/repositories.png" alt="Issues" />
                                        {this.state.data[1].public_repos} repositories
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <span
                            onClick={this.props.buttonCLickReset}
                            className="Reset-button">
                            <Link className="resetToBattle" to="/battle">
                                Reset
                            </Link>
                        </span>
                    </div>
                </React.Fragment >
            );
        }
    }
}

export default Result;