import React, { Component } from "react";
import { Link } from "react-router-dom";
class Battlepage extends Component {

    state = {
        user1: {},
        user2: {},
        displayProfile1: true,
        displayProfile2: true,
        counter: 0
    }

    compareDataForm = (e) => {
        e.preventDefault();
        if (e.target.id === "user1") {
            this.setState({
                counter: this.state.counter + 1
            })
            let inputUser1 = e.target.user1.value;
            this.getUserData(inputUser1, "user1");
        } else if (e.target.id === "user2") {
            this.setState({
                counter: this.state.counter + 1
            })
            let inputUser2 = e.target.user2.value
            this.getUserData(inputUser2, "user2")
        }
    }


    getUserData = (username, user) => {
        fetch(`https://api.github.com/users/${username}`)
            .then((res) => res.json())
            .then((data) => {
                if (user === "user1") {
                    this.profileStateChanger1()
                    this.setState({ user1: data })
                }
                else if (user === "user2") {
                    this.profileStateChanger2()
                    this.setState({ user2: data })
                }
            }
            )
    }


    // , () => this.props.fetchingData(this.state.inputs)
    profileStateChanger1 = () => {
        this.setState({
            displayProfile1: !this.state.displayProfile1
        })
    }

    profileStateChanger2 = () => {
        this.setState({
            displayProfile2: !this.state.displayProfile2
        })
    }

    counterDecrement = () => {
        this.setState({
            counter: this.state.counter - 1
        })
    }

    render() {
        console.log(this.state)

        return (
            <div className="battle-body">
                {/* <header>
                    <nav className="battle-nav">
                        <ul className="battle-nav-ul">
                            <li className="battle-nav-ul-li">
                                <input name="nav-input" type="radio" id="Popular" />
                                <label htmlFor="Popular"><Link style={{ color: this.props.onThemeChange ? 'black' : 'white' }} className="battleLinks" to="/">Popular</Link></label>
                            </li>
                            <li className="battle-nav-ul-li">
                                <input type="radio" name="nav-input" id="Battle" />
                                <label htmlFor="Battle"><Link className="battleLinks" style={{ color: "rgb(187, 46, 31)" }} to="/battle">Battle</Link></label>
                            </li>
                        </ul>
                        <button onClick={this.props.theme} className="battle-torch">{this.props.onThemeChange ? '🔦' : '💡'}</button>
                    </nav>
        </header> */}

                <section className="battle-section">
                    <h1 className="battle-section-h1">Instructions</h1>
                    <ol className="battle-ol">
                        <li className="battle-ol-li">
                            <h3 className="battle-h3">Enter two Github users</h3>
                            <img className={this.props.onThemeChange ? "battle-section-img-bg-light" : "battle-section-img-bg-dark"} src="logos/battle-user.png" alt="" />
                        </li>
                        <li className="battle-ol-li">
                            <h3 className="battle-h3">Battle</h3>
                            <img className={this.props.onThemeChange ? "battle-section-img-bg-light" : "battle-section-img-bg-dark"} src="logos/battle-jet.png" alt="" />
                        </li>
                        <li className="battle-ol-li">
                            <h3 className="battle-h3">See the winner</h3>
                            <img className={this.props.onThemeChange ? "battle-section-img-bg-light" : "battle-section-img-bg-dark"} src="logos/battle-trophy.png" alt="" />
                        </li>
                    </ol>
                </section>

                <footer className="battle-footer">
                    <h1 className="battle-footer-h1">Players</h1>
                    <div className="battle-div-outer">
                        {this.state.displayProfile1 ?
                            <form
                                id="user1"
                                onSubmit={this.compareDataForm}
                                className="battle-form"
                            >
                                <label className="battle-label">Player One</label>
                                <div className="battle-div-inner">
                                    <input id="user1" className={this.props.onThemeChange ? "battle-footer-input-light" : "battle-footer-input-dark"} type="text" placeholder="github username" />
                                    <button className="battle-footer-button">Submit</button>
                                </div>
                            </form> :
                            <div className="playerInfo-Outer-div">
                                <label className="battle-label">Player One</label>
                                <div className="playerInfo-div">
                                    <img className="playerInfo-img" src={this.state.user1.avatar_url} alt="userimg" />
                                    <span className="playerInfo-span">{this.state.user1.name}</span>
                                    <button onClick={() => { this.profileStateChanger1(); this.counterDecrement() }} className="playerInfo-X-Button">X</button>
                                </div>
                            </div>}

                        {this.state.displayProfile2 ?
                            <form
                                id="user2"
                                onSubmit={this.compareDataForm}
                                className="battle-form">
                                <label className="battle-label">Player Two</label>
                                <div className="battle-div-inner">
                                    <input id="user2" className={this.props.onThemeChange ? "battle-footer-input-light" : "battle-footer-input-dark"} type="text" placeholder="github username" />
                                    <button
                                        className="battle-footer-button"
                                    >Submit</button>
                                </div>
                            </form> :
                            <div className="playerInfo-Outer-div">
                                <label className="battle-label">Player Two</label>
                                <div className="playerInfo-div">
                                    <img className="playerInfo-img" src={this.state.user2.avatar_url} alt="userimg" />
                                    <span className="playerInfo-span">{this.state.user2.name}</span>
                                    <button onClick={() => { this.profileStateChanger2(); this.counterDecrement() }} className="playerInfo-X-Button">X</button>
                                </div>
                            </div>}

                    </div>
                    <button
                        onClick={this.props.buttonCLick}
                        className="doBattle"
                        style={{ display: this.state.counter === 2 ? "block" : "none", color: "#e6e6e6" }}
                    >
                        <Link className="doBattleLink" to={`/result/${this.state.user1.login}/${this.state.user2.login}`}>
                            Battle
                        </Link>
                    </button>
                </footer>
            </div >

        );
    }
}

export default Battlepage;